import scala.annotation.tailrec

case class Automaton(f: Function) {
  if (f.numVars < 2)
    throw new Error("Number of variables < 2!")

  case class State(states: List[String]) {
    val successors: Array[State] = Array.tabulate(f.dimension)(_ => null)

    override def toString: String = s"State ${this.hashCode()}: \n" +
      s"values: ${states.sorted} \n" +
      s"succesors: ${(for (i <- successors.indices) yield s"${BigInt(i).toString(f.dimension)} -> ${if (successors(i) != null) successors(i).hashCode() else "null"}").mkString("; ")} \n"
  }

//  val Q: List[String] = List.tabulate(
//    math.pow(f.dimension, f.numVars - 1).toInt
//  )(x =>
//    "0" * (f.numVars - 1 - BigInt(x).toString(f.dimension).length) + BigInt(x).toString(f.dimension)
//  )

  val Q0: List[String] = List.tabulate(
    math.pow(f.dimension, f.numVars / 2).toInt
  )(x =>
    "0" * (f.numVars - 1 - BigInt(x).toString(f.dimension).length) + BigInt(x).toString(f.dimension)
  )

  //  print(1)

  def buildGraph(V0: State): List[State] = {

    @tailrec
    def step(processed: List[State], unprocessed: List[State]): List[State] = {
      if (unprocessed == Nil)
        processed
      else {
        val s = unprocessed.head
        val a: Array[List[String]] = Array.tabulate(f.dimension)(_ => Nil)
        for (x <- s.states)
          for (y <- 0 until f.dimension) {
            val z = BigInt(y).toString(f.dimension)
            if (!a(f.eval(x + z)).contains((x + z).tail))
              a(f.eval(x + z)) = (x + z).tail +: a(f.eval(x + z))
          }

        val r1 = a.map { x =>
          processed.
            find(_.states.sorted == x.sorted).
            getOrElse(
              unprocessed.
                find(_.states.sorted == x.sorted).
                getOrElse(State(x))
            )
        }

        val r = r1.map(x => r1.find(_.states.sorted == x.states.sorted).get)

        for (i <- r.indices)
          s.successors(i) = r(i)

        step(s +: processed,
          r.toSet.filterNot(processed.contains(_)).filterNot(unprocessed.contains(_)).toList ++ unprocessed.tail)
      }
    }

    step(Nil, List(V0))
  }

  lazy val graph: List[State] = buildGraph(State(Q0))

  lazy val childs: LazyList[Array[State]] =
    LazyList.iterate(Array(graph.find(_.states.sorted == Q0.sorted).get))(
      _.flatMap(_.successors).distinct
    )


  def getChilds(s: State, l: Int): List[State] = {
    @tailrec
    def iterate(ss: List[State], ll: Int): List[State] = {
      ll match {
        case 1 => ss.flatMap(_.successors).distinct
        case 0 => ss
        case _ => iterate(ss.flatMap(_.successors).distinct, ll - 1)
      }
    }

    iterate(s.successors.toList.distinct, l - 1)
  }

  def reversibleZero(l: Int): Boolean = {
    def check(s: List[State]): Boolean = !s.exists(_.states == Nil) && s.forall(_.states.exists(_.endsWith("0" * (f.numVars / 2))))

//        check(graph) || check(getChilds(graph.find(_.states.sorted == Q0.sorted).get, l))
    check(graph) || check(childs(l).toList)
  }

}