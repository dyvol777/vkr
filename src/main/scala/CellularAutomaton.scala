object Boundary extends Enumeration {
  type Boundary = Value
  val ZERO, PERIODIC = Value
}

abstract class CellularAutomaton(val function: Function,
                                 dimX: Int,
                                 dimY: Int,
                                 boundary: Boundary.Value) {
  def getNeighbours(i: Int, s: String, dimX: Int, dimY: Int): String

  private val n = math.pow(function.dimension, dimX * dimY).toLong //количество внутренних состояний

  def nextState(s: String, dx: Int, dy: Int): String = {
    val news = s.indices.map(
      i => {
        val q: String = getNeighbours(i, s, dx, dy)
        function.eval("0" * (function.numVars - BigInt(q, function.dimension).toString(function.dimension).length) + BigInt(q, function.dimension).toString(function.dimension))
      }
    ).mkString("")


    assert(news.length == dx * dy)
    news
  }

  lazy val reversible: Boolean = {
    (0L until n).
      map(BigInt(_).toString(function.dimension)).
      map(state => "0" * (dimX * dimY - state.length) + state).
      map(nextState(_, dimX, dimY)).
      toSet.
      size == n
  }

  def modify(s: String): String = {
    val q1 = s.substring(0, dimY)
    val q2 = s.substring(dimY, 2 * dimY)
    val q3 = s.substring(2 * dimY, 3 * dimY)

    var newS = ""
    for (i <- 0 until dimY)
      newS = newS + q1(i) + q2(i) + q3(i)
    newS
  }

  def transformToOneDimension: CellularAutomaton = {
    if (dimY == 1)
      this
    else {
      val k = math.pow(function.dimension, 3 * dimY).toInt
      val newD = math.pow(function.dimension, dimY).toInt

      // *** индексация
      val f = (0 until k).
        map(BigInt(_).toString(function.dimension)).
        map(state => "0" * (3 * dimY - state.length) + state).
        map(modify).
        map(nextState(_, 3, dimY)).
        map(_.zipWithIndex.filter(_._2 % 3 == 1).map(_._1).mkString("")).
        //        map(_.reverse).
        map(BigInt(_, function.dimension).toString(newD)).
        mkString("")


      new CellularAutomaton(Function("0" * (math.pow(newD, 3).toInt - f.length) + f, newD), dimX, 1, boundary) {
        override def getNeighbours(i: Int, s: String, dimX: Int, dimY: Int): String =
          if (boundary == Boundary.PERIODIC)
            s"${s((i - 1 + dimX) % dimX)}" +
              s"${s(i)}" +
              s"${s((i + 1 + dimX) % dimX)}"
          else
            s"${if (i - 1 >= 0) s(i - 1) else 0}" +
              s"${s(i)}" +
              s"${if (i + 1 < dimX) s(i + 1) else 0}"
      }
    }
  }

  override def toString: String = {
    val t = System.nanoTime()
    s"Automate: dimX = $dimX, dimY = $dimY, boundary = $boundary, " +
      s"$function, reversible = $reversible, " +
      s"evaluation time = ${(System.nanoTime() - t) / math.pow(10, 9)}"
  }
}
