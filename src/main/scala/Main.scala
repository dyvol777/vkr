import scala.collection.parallel.CollectionConverters._

class A3(function: Function, dimX: Int, dimY: Int, boundary: Boundary.Value)
  extends CellularAutomaton(function: Function, dimX: Int, dimY: Int, boundary: Boundary.Value) {
  override def getNeighbours(i: Int, s: String, dimX: Int, dimY: Int): String =
    if (boundary == Boundary.PERIODIC) {

      s"${s((i - 2 + dimX) % dimX)}" +
        s"${s(i)}" +
        s"${s((i + 1 + dimX) % dimX)}"


    } else {
      s"${if (i - 1 >= 0) s(i - 1) else 0}" +
        s"${s(i)}" +
        s"${if (i + 1 < dimX) s(i + 1) else 0}"
    }
}

class A5(function: Function, dimX: Int, dimY: Int, boundary: Boundary.Value)
  extends CellularAutomaton(function: Function, dimX: Int, dimY: Int, boundary: Boundary.Value) {
  override def getNeighbours(i: Int, s: String, dimX: Int, dimY: Int): String =
    if (boundary == Boundary.PERIODIC) {

      s"${s((i - 1 + dimX) % dimX)}" +
        s"${s((i - 2 + dimX) % dimX)}" +
        s"${s(i)}" +
        s"${s((i + 1 + dimX) % dimX)}" +
        s"${s((i + 2 + dimX) % dimX)}"

    } else {

      s"${if (i - 2 >= 0) s(i - 2) else 0}" +
        s"${if (i - 1 >= 0) s(i - 1) else 0}" +
        s"${s(i)}" +
        s"${if (i + 1 < dimX) s(i + 1) else 0}" +
        s"${if (i + 2 < dimX) s(i + 2) else 0}"

    }
}

object test {
  /**
   * @param l : automaton lenght
   * @param d : inner dimension
   * @param n : func num vars
   */
  def test1(l: Int, d: Int, n: Int): Unit = {
    println(s"test automatons with lenght $l, in dimension $d and function with $n vars")

    val r = new scala.util.Random

    val t = System.nanoTime()
    Range(0, 10).foreach { _ =>
      val f = r.nextLong(math.pow(d, math.pow(d, n).toLong).toLong)
      val func = Function("0" * (math.pow(d, n).toInt - f.toBinaryString.length) + f.toBinaryString, d)
      if (n == 3) {
        val q = new A3(func, l, 1, Boundary.ZERO).reversible
      }
      else if (n == 5) {
        val q = new A5(func, l, 1, Boundary.ZERO).reversible
      }
    }
    println((System.nanoTime() - t) / math.pow(10, 9) / 10)

    val t2 = System.nanoTime()
    Range(0, 10).foreach { _ =>
      val f = r.nextLong(math.pow(d, math.pow(d, n).toLong).toLong)
      val func = Function("0" * (math.pow(d, n).toInt - f.toBinaryString.length) + f.toBinaryString, d)

      val q = Automaton(func).reversibleZero(l)

    }
    println((System.nanoTime() - t2) / math.pow(10, 9) / 10)

  }

  /**
   * @param d : inner dimension
   * @param n : func num vars
   */
  def test2(d: Int, n: Int): Unit = {
    println(s"test automatons with lenght 8 to 16, in dimension $d and function with $n vars")

    val r = new scala.util.Random

    val t = System.nanoTime()

    for (l <- 8 to 12) {
      for (_ <- 0 until 100) {
        val f = r.nextLong(math.pow(d, math.pow(d, n).toLong).toLong)
        val func = Function("0" * (math.pow(d, n).toInt - BigInt(f).toString(d).length) + BigInt(f).toString(d), d)
        if (n == 3) {
          val q = new A3(func, l, 1, Boundary.ZERO).reversible
        }
        else if (n == 5) {
          val q = new A5(func, l, 1, Boundary.ZERO).reversible
        }
      }
    }
    println((System.nanoTime() - t) / math.pow(10, 9))

    val t2 = System.nanoTime()

    for (_ <- 0 until 100) {
      for (l <- 8 to 12) {
        val f = r.nextLong(math.pow(d, math.pow(d, n).toLong).toLong)
        val func = Function("0" * (math.pow(d, n).toInt - BigInt(f).toString(d).length) + BigInt(f).toString(d), d)

        val q = Automaton(func).reversibleZero(l)

      }
    }
    println((System.nanoTime() - t2) / math.pow(10, 9))
  }
}

case object Main extends App {

  //  val f = Function("00001111110000110000111100001111", 2)
  //  val f = Function("01011010", 2)
  //  val a = Automaton(f)
  //  val s = a.graph
  //  println(s.mkString("\n"))

  //
  //  val s = a.getChilds(a.graph.find(_.states.sorted == a.Q0.sorted).get, 1)
  //  println(s.mkString("\n"))
  //  println("=====================")
  //
  //  val c = a.childs(0)
  //  println(c.mkString("\n"))
  //
  //  println(a.reversibleZero(11))

  //  test.test1(20, 2, 5)
  test.test2(3, 5)
}
