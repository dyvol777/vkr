case class Function(function: String, dimension: Int) {
  val numVars: Int = (math.log(function.length) / math.log(dimension)).round.toInt // от скольки переменных фунция

  assert(math.pow(dimension, numVars) == function.length)
  assert(function.forall(x => x.toString.toInt < dimension && x.toString.toInt >= 0))

  def eval(s: String): Int = {
    assert(s.length == numVars)
    function(BigInt(s, dimension).toInt).toString.toInt
  }

  override def toString: String = s"Function $function on dimension $dimension"
}
