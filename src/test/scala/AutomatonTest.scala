import org.scalatest._
import flatspec._
import matchers._

class AutomatonTest extends AnyFlatSpec with should.Matchers {
  "A Automaton" should "eval correct" in {
    for (f <- 0 until 256) {
      val func = Function("0" * (8 - f.toBinaryString.length) + f.toBinaryString, 2)
      Automaton(func).reversibleZero(11) should be(new A3(func, 11, 1, Boundary.ZERO).reversible)
    }
  }
}