import org.scalatest._
import flatspec._
import matchers._


class FunctionTest extends AnyFlatSpec with should.Matchers {
  "A Function" should "eval correct" in {
    val f = Function("0110", 2)
    f.eval("00") should be(0)
    f.eval("01") should be(1)
    f.eval("10") should be(1)
    f.eval("11") should be(0)

    val f2 = Function("000111222", 3)
    f2.eval("00") should be(0)
    f2.eval("01") should be(0)
    f2.eval("02") should be(0)
    f2.eval("10") should be(1)
    f2.eval("11") should be(1)
    f2.eval("12") should be(1)
    f2.eval("20") should be(2)
    f2.eval("21") should be(2)
    f2.eval("22") should be(2)
  }

  it should "throw exceptions if eval argument error" in {
    val f = Function("0110", 2)
    a[AssertionError] should be thrownBy {
      f.eval("111")
    }

    a[AssertionError] should be thrownBy {
      f.eval("1")
    }

    a[NumberFormatException] should be thrownBy {
      f.eval("22")
    }

    a[NumberFormatException] should be thrownBy {
      f.eval("--")
    }
  }

  it should "throw exceptions if create argument error" in {
    a[AssertionError] should be thrownBy {
      Function("01101", 2)
    }

    a[AssertionError] should be thrownBy {
      Function("011", 2)
    }

    a[AssertionError] should be thrownBy {
      Function("0120", 2)
    }

    a[NumberFormatException] should be thrownBy {
      Function("----", 2)
    }
  }

}